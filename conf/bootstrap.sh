#!/usr/bin/env bash

# if apache2 does no exist
# if [ ! -f /etc/apache2/apache2.conf ];
# then

        cp /vagrant/conf/apt/sources.list /etc/apt/

        #add php7 repository
        LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php

        apt-get update

        # some sysutils
        apt-get install debconf-utils mailutils

        # some stuff for apt
        apt-get -y install python-software-properties

        # Install MySQL
        echo 'mysql-server-5.6 mysql-server/root_password password toor' | debconf-set-selections
        echo 'mysql-server-5.6 mysql-server/root_password_again password toor' | debconf-set-selections
        apt-get -y install mysql-client-5.6 mysql-server-5.6


        # Install Apache
        #apt-get -y install apache2

        #Install nginx
        apt-get -y install nginx

        # Install Git
        apt-get -y install git

        # Install PHP 
        #apt-get -y install php5 libapache2-mod-php5 php-apc php5-mysql php5-dev curl
        apt-get -y install php7.0-fpm php7.0-cli php7.0-common php7.0-json php7.0-opcache php7.0-mysql php7.0-phpdbg php7.0-mbstring php7.0-gd php7.0-imap php7.0-ldap php7.0-pgsql php7.0-pspell php7.0-recode php7.0-tidy php7.0-dev php7.0-intl php7.0-gd php7.0-curl php7.0-zip php7.0-xml php-redis

        # Install OpenSSL
        apt-get -y install openssl

        # Install CURL dev package
        apt-get -y install libcurl4-openssl-dev

        # Install PECL HTTP (depends on php-pear, php5-dev, libcurl4-openssl-dev)x
        apt-get -y install make

        # Enable mod_rewrite    
        #a2enmod rewrite

        # Enable SSL
        #a2enmod ssl

        # Add www-data to vagrant group
        usermod -a -G vagrant www-data

        apt-get -y install imagemagick

        #apt-get -y install php5-xsl
        #apt-get -y install php5-curl
        # apt-get -y install php5-imagick
        # apt-get -y install php5-xdebug
        # apt-get -y install php-http

        apt-get -y install redis-server
        
        printf "\n" | pecl install apc
        
        # echo "xdebug.idekey = PHPSTORM" >> /etc/php5/mods-available/xdebug.ini
        # echo "xdebug.remote_enable=1" >> /etc/php5/mods-available/xdebug.ini
        # echo "xdebug.remote_connect_back = On" >> /etc/php5/mods-available/xdebug.ini
        # echo "xdebug.max_nesting_level = 500" >> /etc/php5/mods-available/xdebug.ini

        # Add www-data to vagrant group
        usermod -a -G vagrant www-data
        usermod -a -G www-data vagrant

        # install symfony
        cd /var/www
        # curl -s https://getcomposer.org/installer | php
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        php composer-setup.php --install-dir=/usr/bin --filename=composer
        php -r "unlink('composer-setup.php');"

        if [ ! -d /var/www/laravel ]; 
            then
            composer create-project --prefer-dist laravel/laravel laravel
        fi  
        # php composer.phar update -d symfony
        
        if [ -d /var/www/laravel/vendor ]; 
            then
            composer install
        fi

        wget https://phar.phpunit.de/phpunit.phar
        chmod +x phpunit.phar
        mv phpunit.phar /usr/local/bin/phpunit
        phpunit --version

        #rsync -av /vagrant/conf/apache2/* /etc/apache2/
        rsync -av /vagrant/conf/nginx/conf.d/ /etc/nginx/conf.d/
        rsync -av /vagrant/conf/php/7.0/fpm/pool.d/* /etc/php/7.0/fpm/pool.d/
        rm -rf /etc/nginx/sites-available/*
        rm -rf /etc/nginx/sites-enabled/*
        # service apache2 restart

        locale-gen "en_US.UTF-8"
        dpkg-reconfigure locales

         # And clean up apt-get packages
        apt-get -y clean

        sleep 5
        
        service mysql restart
        service nginx restart
        service php7.0-fpm restart

# fi
