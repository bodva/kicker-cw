<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
	 * @test
     */
    public function AppInfo()
    {
        $this->visit('/telegram')->seeJson()->seeJsonStructure(['id', 'first_name', 'username']);
    }
}
