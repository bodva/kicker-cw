<?php

use App\Exceptions\TooManyUsersException;
use App\Team;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TeamTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp() {
		parent::setUp();
	}

	/** @test */
	public function team_consists_of_users() {
		$team = $this->createTeamWithPlayers();

		$this->assertEquals(2, $team->count());
    }

    protected function createTeamWithPlayers() {
		$team = factory(Team::class)->create();

		$this->addPlayerToTeam($team);
		$this->addPlayerToTeam($team);

		return $team;
	}

	/** @test */
	public function team_has_max_size() {
		$team = factory(Team::class)->create(['size' => 2]);

		$this->addPlayerToTeam($team);
		$this->addPlayerToTeam($team);

		$this->assertEquals(2, $team->count());

		$this->setExpectedException(TooManyUsersException::class);
		$this->addPlayerToTeam($team);
	}
	
//	public function team_has_max_size_when_add_many_at_once() {
//		$team = factory(Team::class)->create(['size' => 2]);
//		$users = factory(User::class, 3)->create();
//
//		$this->setExpectedException('Exception');
//		$team->addPlayer($users);
//	}
//
//	public function team_add_multiple_players() {
//		$team = factory(Team::class)->create(['size' => 2]);
//
//		$users = factory(User::class, 2)->create();
//
//		$team->addPlayer($users);
//
//		$this->assertEquals(2, $team->count());
//	}

	protected function addPlayerToTeam(Team $team) {
		$user = factory(User::class)->create();
		$team->addPlayer($user);
	}
}
