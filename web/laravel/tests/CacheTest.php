<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Cache;

class CacheTest extends TestCase
{
    /**
     * @test
     */
    public function check_default_cache_driver()
    {
        $this->assertEquals('array',Cache::getDefaultDriver());
    }

	/**
	 * @test
	 */
	public function check_cache() {

		Cache::put('name', 'Dmitry', 10);

		$this->assertEquals('Dmitry', Cache::get('name'));
    }
}
