<?php

use App\Game;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp() {
		parent::setUp();
	}

	/** @test */
	public function getting_top_players() {
		factory(User::class, 3)->create();
		factory(User::class)->create(['rating' => 2300]);
		$mostTop = factory(User::class)->create(['rating' => 2400]);

		$users = User::top();

		$this->assertEquals($mostTop->id, $users->first()->id);
		$this->assertCount(3, $users);
	}
}
