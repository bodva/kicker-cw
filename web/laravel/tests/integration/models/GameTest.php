<?php

use App\Game;
use App\Team;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GameTest extends TestCase
{
	use DatabaseTransactions;

	/** @var  Game */
	protected $game;
	/** @var  Team */
	protected $team_a;
	/** @var  Team */
	protected $team_b;


	public function setUp() {
		parent::setUp();
		$this->setUpTeams();
		$this->game = new Game($this->team_a, 10, $this->team_b, 3);
    }

	public function setUpTeams() {
		$this->team_a = factory(Team::class)->create();
		$this->team_b = factory(Team::class)->create();
    }

    /** @test */
	public function game_has_winner_team() {
		$this->assertTrue($this->game->hasWinner());
		$this->assertEquals($this->team_a->id, $this->game->winner->id);
    }

    /** @test  */
	public function game_has_two_team() {
		$this->assertEquals(2, count($this->game->teams));
		$this->assertEquals($this->team_a->id, $this->game->team_a->id);
		$this->assertEquals($this->team_b->id, $this->game->team_b->id);
    }

    /** @test */
	public function winner_team_has_users() {
		$this->assertEquals($this->team_a->userA->id, $this->game->winner->userA->id);
		$this->assertEquals($this->team_a->userB->id, $this->game->winner->userB->id);
	}
}
