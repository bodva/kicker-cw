<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
		'rating' => $faker->numberBetween(1000, 2200),
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Team::class, function (Faker\Generator $faker) {
	return [
		'user_a_id' => factory(\App\User::class)->create()->id,
		'user_b_id' => factory(\App\User::class)->create()->id,
		'size' => 2,
	];
});