<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Game  extends Model
{
	const WINNER_SCORE = 10;

	protected $team_a_score;
	protected $team_b_score;
	protected $team_a_id;
	protected $team_b_id;

	public function __construct(Team $team_a, $score_a, Team $team_b, $score_b) {
		parent::__construct();

		$this->team_a = $team_a;
		$this->team_a_score = $score_a;
		$this->team_b = $team_b;
		$this->team_b_score = $score_b;
	}

	/**
	 * @return bool
	 */
	public function hasWinner() {
		return $this->team_a_score == self::WINNER_SCORE || $this->team_b_score == self::WINNER_SCORE;
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function teamA() {
		return $this->belongsTo(Team::class, 'team_a_id', 'id');
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function teamB() {
    	return $this->belongsTo(Team::class, 'team_b_id', 'id');
	}

	/**
	 * @return Collection
	 */
	public function getTeamsAttribute() {
		return new Collection([$this->team_a, $this->team_b]);
    }

	/**
	 * @return Team
	 */
	public function getWinnerAttribute() {
		return ($this->team_a_score > $this->team_b_score) ? $this->team_a : $this->team_b;
    }
}
