<?php

namespace App\Services;

use App\Messages\MessageInterface;
use Illuminate\Contracts\Foundation\Application;
use App\Services\Contracts\TelegramMessageContract;
use Illuminate\Support\Facades\Log;
use Telegram;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Api;

class TelegramMessage implements TelegramMessageContract{

	/** @var  Message */
	protected $message;

	/**
	 * @var array
	 */
	protected $commands = [];

	/**
	 * @var Api
	 */
	private $telegram;

	/**
	 * TelegramMessage constructor.
	 * @param Api $telegram
	 * @param Application $app
	 */
	public function __construct(Api $telegram, Application $app) {
		$this->telegram = $telegram;

		$config = $app['config'];
		$this->addCommands($config->get('telegram.messages', []));
	}

	/**
	 * @param array $messages
	 * @return $this
	 */
	private function addCommands(array $messages) {
		foreach ($messages as $message) {
			$this->addCommand($message);
		}

		return $this;
	}

	/**
	 * @param $command
	 * @return $this
	 * @throws \Exception
	 */
	private function addCommand($command) {
		if (!is_object($command)) {
			if (!class_exists($command)) {
				throw new \Exception(
					sprintf(
						'Command class "%s" not found! Please make sure the class exists.',
						$command
					)
				);
			}

			$command = new $command;
		}

		if ($command instanceof MessageInterface){
			$this->commands[$command->getText()] = $command;
		} else {
			throw new \Exception(
				sprintf(
					'Command class "%s" should be an instance of "App\Messages\MessageInterface"',
					get_class($command)
				)
			);
		}

		return $this;
	}

	/**
	 * @return $this
	 * @throws \Exception
	 */
	public function handle() {
		if (!! $this->message) {
			Log::info('MessageText: '. $this->message->getText());
			$this->handleMessage();
		} else {
			throw new \Exception('Need to set Telegram Message at first', 412);
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	private function handleMessage() {
		$message_text = mb_strtolower($this->message->getText());
		$words = explode(' ', $message_text);
		foreach ($words as $word) {
			if (key_exists($word, $this->commands)) {
				$this->commands[$word]->handle($this->message);
			}
		}
		return $this;
	}

	/**
	 * @param Message $message
	 * @return TelegramMessage
	 */
	public function setMessage(Message $message) {
		$this->message = $message;

		return $this;
	}
}