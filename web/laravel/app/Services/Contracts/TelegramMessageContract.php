<?php

namespace App\Services\Contracts;

use Telegram\Bot\Objects\Message;

interface TelegramMessageContract {

	/**
	 * @return mixed
	 */
	public function handle();

	/**
	 * @param Message $message
	 * @return mixed
	 */
	public function setMessage(Message $message);

}