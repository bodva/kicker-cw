<?php

namespace App\Messages\Add;

use App\Messages\Message;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram;
use Telegram\Bot\Objects\Message as TelegramMessage;

class AllyMessage extends Message{

	/**
	 * @var string
	 */
	protected $text = 'ally';

	public function handle(TelegramMessage $telegram_message) {
		parent::handle($telegram_message);

		Log::info('Ally text handled');

		$this->sendMessage('Now you can add your ally');

		return $this;
	}
}