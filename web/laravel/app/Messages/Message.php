<?php

namespace App\Messages;

use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram;
use Telegram\Bot\Objects\Message as TelegramMessage;

class Message implements MessageInterface {

	/**
	 * @var TelegramMessage
	 */
	protected $telegram_message;

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getText() {
		if (isset($this->text)) {
			return $this->text;
		} else {
			throw new \Exception('Message cannot be empty');
		}
	}

	/**
	 * @param Api $telegram
	 * @param TelegramMessage $message
	 * @return $this
	 */
	public function handle(TelegramMessage $message) {
		$this->telegram_message = $message;

		return $this;
	}

	protected function sendMessage($text) {
		$chat_id = $this->telegram_message->getChat()->getId();
		$reply = [
			'chat_id' => $chat_id,
			'text'    => $text,
		];

		Log::info($reply);

		Telegram::sendMessage($reply);

		return $this;
	}
}