<?php

namespace App\Messages;

use Telegram\Bot\Objects\Message;

interface MessageInterface {
	/**
	 * @return string
	 */
	public function getText();

	/**
	 * @param Message $message
	 * @return mixed
	 */
	public function handle(Message $message);
}