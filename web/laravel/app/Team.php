<?php

namespace App;

use App\Exceptions\TooManyUsersException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Team  extends Model
{
	/**
	 * @param User $user
	 */
	public function addPlayer(User $user) {

		$this->guardAgainstTooManyMembers();

		if (!$this->user_a) {
			$this->user_a = $user;
			$this->user_a->save();
		} else {
			$this->user_b = $user;
			$this->user_b->save();
		}
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function userA() {
		return $this->belongsTo(User::class, 'user_a_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function userB() {
		return $this->belongsTo(User::class, 'user_b_id', 'id');
	}

	/**
	 * @return Collection
	 */
	public function players() {
		return new Collection([$this->user_a, $this->user_b]);
    }

	/**
	 * @return int
	 */
	public function count() {
		return $this->players()->count();
    }

	/**
	 * @throws TooManyUsersException
	 */
    protected function guardAgainstTooManyMembers() {
		if ($this->user_a AND $this->user_b) {
			throw new TooManyUsersException();
		}
	}
}
