<?php

namespace App\Http\Controllers;

use ApiAi\Client as ApiAiCliend;
use Tgallice\Wit\Client as WitClient;
use Illuminate\Http\Request;

use App\Http\Requests;

class LangController extends Controller
{
	public function apiai(Request $request) {
		$token = getenv('APIAI_TOKEN');

		try {
			$text = $request->get('text');
			$client = new ApiAiCliend($token);

			$query = $client->get('query', [
				'lang' => 'en',
				'query' => $text,
			]);

			$response = json_decode((string) $query->getBody(), true);
		} catch (\Exception $e) {
			throw new \Exception($e->getMessage(), $e->getCode());
		}

		return $response;

    }

	public function wit(Request $request) {
		$token = getenv('WIT_TOKEN');

		try {
			$text = $request->get('text');
			$client = new WitClient($token);

			$query = $client->get('/message', [
				'lang' => 'en',
				'q' => $text,
			]);

			$response = json_decode((string) $query->getBody(), true);
		} catch (\Exception $e) {
			throw new \Exception($e->getMessage(), $e->getCode());
		}

		return $response;

	}

}
