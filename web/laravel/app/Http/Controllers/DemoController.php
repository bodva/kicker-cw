<?php

namespace App\Http\Controllers;

use App\Helpers\Contracts\RocketShipContract;

class DemoController extends Controller
{
	public function index(RocketShipContract $rocket) {
		$boom = $rocket->blastOff();

		return $boom;
    }
}
