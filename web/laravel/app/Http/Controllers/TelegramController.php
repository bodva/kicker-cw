<?php

namespace App\Http\Controllers;

use App\Services\Contracts\TelegramMessageContract;
use Telegram;

class TelegramController extends Controller
{
	public function index() {
		$response = Telegram::getMe();

		$botId = $response->getId();
		$firstName = $response->getFirstName();
		$username = $response->getUsername();

		return $response;
    }

	public function addCommands() {
		$response = Telegram::addCommands([
			\Telegram\Bot\Commands\HelpCommand::class,
			\App\Commands\StartCommand::class,
			\App\Commands\ListCommand::class,
			\App\Commands\AddCommand::class,
		]);

		dd($response);
	}

	public function getUpdates() {
		$response = Telegram::getUpdates();

		return $response;
	}

	public function setWebhook() {
		$url = env('APP_URL');
		$response = Telegram::setWebhook(['url' => $url . '/webhook']);
		
		return	$response;
	}

	public function incoming(TelegramMessageContract $telegram_message) {
		$updates = Telegram::commandsHandler(true);

		$updates_array = json_decode($updates, true);
		\Log::info('Webhook: ', $updates_array);

		$message = $updates->getMessage();

		if ($message !== null && $message->has('text')) {
			$telegram_message->setMessage($message);

			$telegram_message->handle();

//			Telegram::replyKeyboardHide();
		}

		return 'ok';
	}

	public function demo(TelegramMessageContract $telegram_message) {
		dd($telegram_message);
	}

	public function sendMessage($chat_id, $message) {

		$response = Telegram::sendMessage([
			'chat_id' => $chat_id,
			'text' => $message
		]);

		return $response;
	}
}
