<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Services\Contracts\TelegramMessageContract;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/telegram', 'TelegramController@index');
Route::get('/telegram/updates', 'TelegramController@getUpdates');
Route::get('/telegram/set-webhook', 'TelegramController@setWebhook');
Route::get('/telegram/add-commands', 'TelegramController@addCommands');

Route::post('/webhook', 'TelegramController@incoming');

Route::get('/demo', 'DemoController@index');

Route::get('/telegram/demo', 'TelegramController@demo');

Route::get('/apiai', 'LangController@apiai');
Route::get('/wit', 'LangController@wit');