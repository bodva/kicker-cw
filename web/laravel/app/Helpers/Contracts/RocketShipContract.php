<?php

namespace App\Helpers\Contracts;

interface RocketShipContract {

	public function blastOff();

}