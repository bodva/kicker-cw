<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class AddCommand extends Command {

	/**
	 * @var string Command Name
	 */
	protected $name = "add";

	/**
	 * @var string Command Description
	 */
	protected $description = "Add game command";

	/**
	 * @inheritdoc
	 */
	public function handle($arguments) {
		// This will update the chat status to typing...
		$this->replyWithChatAction(['action' => Actions::TYPING]);

		$keyboard = [
			['Ally', 'Enemy'],
		];
		$params = [
			'keyboard'          => $keyboard,
			'resize_keyboard'   => true,
			'one_time_keyboard' => true,
		];
		$reply_markup = \Telegram::replyKeyboardMarkup($params);
		\Log::info('markup: ' . $reply_markup);

		$this->replyWithMessage(['text' => 'This command for adding game', 'reply_markup' => $reply_markup]);
	}

}