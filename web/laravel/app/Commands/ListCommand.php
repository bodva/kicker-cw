<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class ListCommand extends Command{

	/**
	 * @var string Command Name
	 */
	protected $name = "list";

	/**
	 * @var string Command Description
	 */
	protected $description = "List of players";

	/**
	 * @inheritdoc
	 */
	public function handle($arguments)
	{
		// This will update the chat status to typing...
		$this->replyWithChatAction(['action' => Actions::TYPING]);

		$this->replyWithMessage(['text' => 'This is list of users']);
	}

}