<?php

namespace App\Providers;

use App\Services\Contracts\TelegramMessageContract;
use App\Services\TelegramMessage;
use Illuminate\Support\ServiceProvider;
use Telegram\Bot\Api;

class TelegramMessageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->bind(TelegramMessageContract::class, function() {
		    return new TelegramMessage(new Api, $this->app);
	    });
    }
}
